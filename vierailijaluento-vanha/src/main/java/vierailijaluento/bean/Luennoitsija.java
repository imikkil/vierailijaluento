package vierailijaluento.bean;

public class Luennoitsija {
	
	private int id;
	private String Etunimi;
	private String Sukunimi;
	private String kokoNimi;
	
	public Luennoitsija() {
		super();
		
	}

	public Luennoitsija(int id, String etunimi, String sukunimi) {
		super();
		this.id = id;
		Etunimi = etunimi;
		Sukunimi = sukunimi;
		this.kokoNimi = Etunimi + " " + Sukunimi;
	}

	public Luennoitsija(String etunimi, String sukunimi) {
		this.Etunimi = etunimi;
		this.Sukunimi = sukunimi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEtunimi() {
		return Etunimi;
	}

	public void setEtunimi(String etunimi) {
		Etunimi = etunimi;
	}

	public String getSukunimi() {
		return Sukunimi;
	}

	public void setSukunimi(String sukunimi) {
		Sukunimi = sukunimi;
	}

	public String getKokoNimi() {
		return kokoNimi;
	}

	public void setKokoNimi(String kokoNimi) {
		this.kokoNimi = kokoNimi;
	}

	@Override
	public String toString() {
		return "Luennoitsija [id=" + id + ", Etunimi=" + Etunimi
				+ ", Sukunimi=" + Sukunimi + ", kokoNimi=" + kokoNimi + "]";
	}

	
	
	
}
