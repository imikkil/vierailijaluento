package vierailijaluento.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import vierailijaluento.bean.Luennoitsija;



@Repository
public class LuennoitsijaDAO {
	
	
	@Inject
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Inject
	private LuennoitsijaRowMapper luennoitsijamapper;
	
	public LuennoitsijaRowMapper getLuennoitsijaRowMapper(){
		return luennoitsijamapper;
	}
	
	public void setLuennoitsijaRowMapper(LuennoitsijaRowMapper luennoitsijamapper){
		this.luennoitsijamapper = luennoitsijamapper;
	}
	
	public List<Luennoitsija> haeKaikki(){
		// toimiikohan rowmapper wildcardilla
		String sqlQuery = "SELECT * FROM luennoitsija;";
		List<Luennoitsija> luennoitsijalista = jdbcTemplate.query(sqlQuery,luennoitsijamapper);
		
		return luennoitsijalista;
		
	}
	
	public Luennoitsija haeId(int id){
		String sqlQuery = "SELECT * FROM luennoitsija WHERE id =?;";
		Object[] parametrit = new Object[] { id };
		Luennoitsija luennoitsija = null;
		try {
			luennoitsija = jdbcTemplate.queryForObject(sqlQuery, parametrit, luennoitsijamapper);
		} catch (IncorrectResultSizeDataAccessException e) {
			System.out.println("luennoitsijan haku ei onnistu: "+ e);
		}
		
		return luennoitsija;
	}
	
	public Luennoitsija haeNimella(String nimi){
		
		String etunimi = nimi.substring(0, nimi.indexOf(" ")).trim();
		String sukunimi = nimi.substring(nimi.indexOf(" "),nimi.length()).trim();
		String sqlQuery = "SELECT * FROM luennoitsija WHERE etunimi=? and sukunimi=?;";
		Object[] parametrit = new Object[] { etunimi, sukunimi };
		Luennoitsija luennoitsija = null;
		try {
			luennoitsija = jdbcTemplate.queryForObject(sqlQuery, parametrit, luennoitsijamapper);
		} catch (IncorrectResultSizeDataAccessException e) {
			System.out.println("luennoitsijan haku ei onnistu: "+ e);
		}
		if (luennoitsija == null){
			Lisaaluennoitsija(new Luennoitsija(etunimi, sukunimi));
			luennoitsija = haeNimella(nimi);
		}
		
		return luennoitsija;
	}
	
	public void Lisaaluennoitsija(Luennoitsija luennoitsija){
		final String sqlupdate = "INSERT INTO luennoitsija(etunimi, sukunimi) VALUES (?, ?);";
		
		final String etunimi = luennoitsija.getEtunimi();
		final String sukunimi = luennoitsija.getSukunimi();
		//id talteen
		KeyHolder idHolder = new GeneratedKeyHolder();
		//p�ivitys preparedstatement creatorilla
		
		jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException{
						PreparedStatement ps = connection.prepareStatement(sqlupdate, new String []{"id"});
						ps.setString(1, etunimi);
						ps.setString(2, sukunimi);
						return ps;
					}
				}, idHolder);
		
		luennoitsija.setId(idHolder.getKey().intValue());	
	}
	
}	
	
