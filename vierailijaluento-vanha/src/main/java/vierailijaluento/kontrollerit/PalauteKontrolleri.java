package vierailijaluento.kontrollerit;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vierailijaluento.bean.Luento;
import vierailijaluento.bean.LuentoPalaute;
import vierailijaluento.dao.LuentoDAO;
import vierailijaluento.dao.LuentoPalauteDAO;
 
@Controller
public class PalauteKontrolleri {
	
	@Inject
	private LuentoPalauteDAO luentopalautedao;
	
	public LuentoPalauteDAO getLuentoPalauteDao(){
		return luentopalautedao;
	}
	
	public void setLuentoPalauteDao(LuentoPalauteDAO luentopalautedao){
		this.luentopalautedao = luentopalautedao;
	}
	
	@Inject
	private LuentoDAO luentodao;
	
	public LuentoDAO getLuentoDao(){
		return luentodao;
	}
	
	public void setLuentoDao(LuentoDAO luentodao){
		this.luentodao = luentodao;
	}

 
	@RequestMapping(value = {"/palaute", "/palaute/", "/palaute?luentokoodi={luentokoodi}", "/palaute/?luentokoodi={luentokoodi}"}, method=RequestMethod.GET)
	public String palauteGet(Model model, @RequestParam(value="luentokoodi") String koodi, @ModelAttribute String koodiStr){
		System.out.println(koodiStr);
		Luento luento = luentodao.haeKoodi(koodi);
		if(luento == null){
			model.addAttribute("luentovirhe", "true");
			return "index";
		}
		LuentoPalaute luentopalaute = new LuentoPalaute();
		model.addAttribute("luentopalaute", luentopalaute);
		model.addAttribute("luento", luento);
		return "nayta-luento";
		
	}
	
	@RequestMapping(value={"/palaute/", "/palaute"}, method=RequestMethod.POST)
	public String palautePost(Model model, @ModelAttribute LuentoPalaute luentopalaute){
		System.out.println("PalauteKontrolleri.palautePost()");
		System.out.println(luentopalaute);
		luentopalautedao.lisaa(luentopalaute);
		
		model.addAttribute("luentopalaute", "true");
		return "index";
	}
	
}