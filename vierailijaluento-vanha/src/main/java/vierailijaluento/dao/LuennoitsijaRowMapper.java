package vierailijaluento.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import vierailijaluento.bean.Luennoitsija;

public class LuennoitsijaRowMapper implements RowMapper<Luennoitsija> {

	public Luennoitsija mapRow(ResultSet rs, int rivirno) throws SQLException {
		Luennoitsija luennoitsija = new Luennoitsija();
		luennoitsija.setEtunimi(rs.getString("etunimi"));
		luennoitsija.setSukunimi(rs.getString("sukunimi"));
		luennoitsija.setId(rs.getInt("id"));
		luennoitsija.setKokoNimi(rs.getString("etunimi") + " " + rs.getString("sukunimi"));
		return luennoitsija;
	}

}
