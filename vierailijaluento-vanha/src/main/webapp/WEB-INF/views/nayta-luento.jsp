<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="http://s3.amazonaws.com/codecademy-content/courses/ltp/css/bootstrap.css">
<link href="<c:url value='/styles/main.css' /> " rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Luentopalaute &middot; Vierailijaluennot</title>
</head>
<body>
<div class="wrapper">
<h1 class="header">Luentopalaute</h1>
<div class="container">
	<p>Anna arviosi luennosta</p> <br>
	<table id="luennot">
		<thead>
			<tr id="tauluhead">
				<th class="taulu"><b>Luennon aihe</b></th>
				<th class="taulu"><b>Luennon pvm</b></th>
			</tr>
		</thead>
		<tbody> 
				<td class="taulu taulureuna"><c:out value="${luento.luennonaihe }"/></td>
				<td class="taulu"><c:out value='${luento.ajankohta}'/></td>	
		</tbody>
	</table>
</div>

<div class="container">

<form:form method="post" modelAttribute="luentopalaute">
	<form:input type="hidden" name="luentoid" value="${luento.id}" path="luentoid" />
	Syötä opiskelijanumerosi:<br>
	<form:input type="text" size="8" placeholder=""  maxlength="8" name="opiskelijaid" path="oppilasid" /><br>
	Oliko kokemuksesi positiivinen:<br>
	<form:radiobutton name="peukku" value="true" path="peukku" /> Kyllä
 	<form:radiobutton name="peukku" value="false" path="peukku" /> Ei<br>
	Anna palautetta luennosta:<br>
	<form:textarea rows="4" cols="30" name="kommentti" path="kommentti" /> <br>
	
	<input type="submit" value="Tallenna" />
	
</form:form>
</div>
<c:if test="${virheet != null } && ${!virheet.isEmpty()}">
<div class="virhe">
	<c:forEach items="${virheet}" var="virhe">
	<c:out value="${virhe.value}"/>
	</c:forEach>
</div>
</c:if>

</div>
<%@ include file="footer.inc" %>
</body>
</html>