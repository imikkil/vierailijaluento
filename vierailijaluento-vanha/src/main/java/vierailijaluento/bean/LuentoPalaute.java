package vierailijaluento.bean;

public class LuentoPalaute {
	
	private int id;
	private int oppilasid;
	private int luentoid;
	private String kommentti;
	private boolean peukku;
	
	public LuentoPalaute(){
		super();
	}
	public LuentoPalaute(int oppilasid, int luentoid, String kommentti,
			boolean peukku) {
		super();
		this.oppilasid = oppilasid;
		this.luentoid = luentoid;
		this.kommentti = kommentti;
		this.peukku = peukku;
	}

	public LuentoPalaute(int id, int oppilasid, int luentoid, String kommentti,
			boolean peukku) {
		super();
		this.id = id;
		this.oppilasid = oppilasid;
		this.luentoid = luentoid;
		this.kommentti = kommentti;
		this.peukku = peukku;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOppilasid() {
		return oppilasid;
	}

	public void setOppilasid(int oppilasid) {
		this.oppilasid = oppilasid;
	}

	public int getLuentoid() {
		return luentoid;
	}

	public void setLuentoid(int luentoid) {
		this.luentoid = luentoid;
	}

	public String getKommentti() {
		return kommentti;
	}

	public void setKommentti(String kommentti) {
		this.kommentti = kommentti;
	}

	public boolean isPeukku() {
		return peukku;
	}

	public void setPeukku(boolean peukku) {
		this.peukku = peukku;
	}

	@Override
	public String toString() {
		return "LuentoPalaute [id=" + id + ", oppilasid=" + oppilasid
				+ ", luentoid=" + luentoid + ", kommentti=" + kommentti
				+ ", peukku=" + peukku + "]";
	}


		
	

}

