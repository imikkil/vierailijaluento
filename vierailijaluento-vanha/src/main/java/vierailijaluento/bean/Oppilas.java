package vierailijaluento.bean;

public class Oppilas {

	private int id;
	private String etunimi;
	private String sukunimi;
	
	public Oppilas() {
		super();
		
	}

	public Oppilas(int id, String etunimi, String sukunimi) {
		super();
		this.id = id;
		this.etunimi = etunimi;
		this.sukunimi = sukunimi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEtunimi() {
		return etunimi;
	}

	public void setEtunimi(String etunimi) {
		this.etunimi = etunimi;
	}

	public String getSukunimi() {
		return sukunimi;
	}

	public void setSukunimi(String sukunimi) {
		this.sukunimi = sukunimi;
	}

	@Override
	public String toString() {
		return "Oppilas [id=" + id + ", etunimi=" + etunimi + ", sukunimi="
				+ sukunimi + "]";
	}
	
	
}
