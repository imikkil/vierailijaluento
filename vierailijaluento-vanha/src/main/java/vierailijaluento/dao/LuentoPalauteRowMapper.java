package vierailijaluento.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import vierailijaluento.bean.LuentoPalaute;


public class LuentoPalauteRowMapper implements RowMapper<LuentoPalaute> {

	public LuentoPalaute mapRow(ResultSet rs, int rivirno) throws SQLException {
		LuentoPalaute luentopalaute = new LuentoPalaute();
		luentopalaute.setKommentti(rs.getString("kommentti"));
		luentopalaute.setPeukku(rs.getBoolean("peukku"));
		luentopalaute.setId(rs.getInt("id"));
		luentopalaute.setLuentoid(rs.getInt("luentoid"));
		luentopalaute.setOppilasid(rs.getInt("oppilasid"));
		return luentopalaute;
	}

}
