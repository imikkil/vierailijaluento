package vierailijaluento.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;

import org.springframework.jdbc.core.RowMapper;

import vierailijaluento.bean.Luento;



public class LuentoRowMapper implements RowMapper<Luento> {
	@Inject
	private LuennoitsijaDAO luennoitsijadao;
	@Inject
	private OpettajaDAO opedao;

	public LuennoitsijaDAO getLuennoitsijaDao() {
		return luennoitsijadao;
	}

	public void setLuennoitsijaDao(LuennoitsijaDAO luennoitsijadao) {
		this.luennoitsijadao = luennoitsijadao;
	}
	public OpettajaDAO getOpettajaDao() {
		return opedao;
	}

	public void setOpettajaDao(OpettajaDAO opedao) {
		this.opedao = opedao;
	}
	
	
	public Luento mapRow(ResultSet rs, int rowNum) throws SQLException {
	
		Luento luento = new Luento();
		luento.setLuennonaihe(rs.getString("luennonaihe"));
		luento.setAjankohta(rs.getTimestamp("pvm"));
		luento.setId(rs.getInt("id"));
		luento.setKoodi(rs.getString("koodi"));
		luento.setPaikka(rs.getString("paikka"));
		luento.setLuennoitsija(luennoitsijadao.haeId(rs.getInt("luennoitsijaid"))); //T�� ja seuraava rivi kolminkertaistaa db-yhteydet luentoja haettaessa
		luento.setOpettaja(opedao.haeId(rs.getInt("vastuuopettajaid"))); //OH NOES :((((
		return luento;
	}

}

	