<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<!-- Väliaikainen bootstrap linkki kokeilua varten,
kokeiltu toistaiseksi navbar.inc kanssa -->
<link rel="stylesheet" href="http://s3.amazonaws.com/codecademy-content/courses/ltp/css/bootstrap.css">


<link href="styles/main.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script src="javascript/script.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Vierailijaluennot</title>
</head>
<body>
<div class="wrapper">
<%@ include file="header.inc" %>
<div class="nav">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="navpad"><a href="<c:url value='/' /> ">Etusivu</a></div>
		</div>
	</div>
</div>
</div>

<div class="container">
	<form:form modelAttribute="opettaja" method="post">
		&nbsp;&nbsp;&nbsp;Etunimi:<form:input path="etunimi" value=""/>
		<br>Sukunimi:<form:input path="sukunimi" value=""/>
		<br>Salasana:<form:input  type="password" path="salasana" value=""/>
		<br><input type="submit" value="Rekisteröidy">
	</form:form>
</div>
<c:if test="${not empty rekisteroitymisvirhe}">
<div class="virhe">
	<p>Olet jo rekisteröitynyt järjestelmään. Jos olet unohtanut salasanasi tai tunnuksesi ota yhteyttä järjestelmänvalvojaan.</p>
</div>
</c:if>
<c:if test="${virheet != null } && ${!virheet.isEmpty()}">
<div class="virhe">
	<c:forEach items="${virheet}" var="virhe">
	<c:out value="${virhe.value}"/>
	</c:forEach>
</div>
</c:if>
</div>
<%@ include file="footer.inc" %>
</body>
</html>
