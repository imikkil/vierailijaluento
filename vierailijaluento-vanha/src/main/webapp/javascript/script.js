
//dropdown-menuun liitetään dokumentin lataamisen jälkeen onchange-handler
$(document).ready(function(){
	document.getElementById("aika").onchange = function() {
		//kutsutaan oikeaa funktiota riippuen mitä valitaan dropdownista
		if(this.value=="kk"){
			viimeinenKuukausi();
		}else if(this.value=="vuosi"){
			viimeinenVuosi();
		}else{
			oletus();
		}
	}

})
//piilotetaan taulusta kaikki yli kuukauden vanhat luennot
function viimeinenKuukausi(){
	//palautetaan tauluun ensin kaikki luennot
	oletus();
	//luodaan date-muuttuja, jonka pvm on kuukauden vanha
	var kuukausiSitten = new Date();
	kuukausiSitten.setMonth(kuukausiSitten.getMonth() - 1);
	
	//Alustetaan date-muuttuja johon sitten haetaan luennoista pvm
	var vertailuAika = new Date();
	
	//Käydään läpi koko luentolista ja piilotetaan kuukauden vanhat luennot
	for(var i = 0; i < luentolista.length; i++){
		vertailuAika.setTime(Date.parse(luentolista[i].ajankohta));
		console.log(vertailuAika);
		var luentoTemp = luentolista[i];
		var idTemp = luentoTemp.id;
		if(vertailuAika < kuukausiSitten){
			console.log("Vertailuaika pienempi!");
			var elementTemp = document.getElementById(idTemp);
			//virhesuojausta, jos elementtiä ei löydykään, ei tehdä sille mitään
			if(elementTemp){
				$('#' + idTemp).hide();
			}
			
		}
	}
	
	
                   
}
//katso kommentit ylempää, muutoin sama, mutta katsotaan vuotta
function viimeinenVuosi(){
	oletus();
	var vuosiSitten = new Date();
	vuosiSitten.setFullYear(vuosiSitten.getFullYear() - 1);

	var vertailuAika = new Date();
	
	
	for(var i = 0; i < luentolista.length; i++){
		vertailuAika.setTime(Date.parse(luentolista[i].ajankohta));
		console.log(vertailuAika);
		var luentoTemp = luentolista[i];
		var idTemp = luentoTemp.id;
		if(vertailuAika < vuosiSitten){
			console.log("Vertailuaika pienempi!");
			var elementTemp = document.getElementById(idTemp);
			if(elementTemp){
				$('#' + idTemp).after('<tr></tr>').hide();
			}
			
		}
	}
	
                   
}

function oletus() {
	var rivit = $('table.luennot tr');
	rivit.show();	
}




                   
                   



