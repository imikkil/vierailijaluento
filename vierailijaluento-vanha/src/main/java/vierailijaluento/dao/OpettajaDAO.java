package vierailijaluento.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import vierailijaluento.bean.Luennoitsija;
import vierailijaluento.bean.Opettaja;

@Repository
public class OpettajaDAO {
	
	
	//k�ytet��n springframeworkin jdbctemplatea tietokantayhteyden muodostamiseen 
	@Inject
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Inject
	private OpettajaRowMapper opemapper;
	
	public OpettajaRowMapper getOpettajaRowMapper(){
		return opemapper;
	}
	
	public void setOpettajaRowMapper(OpettajaRowMapper opemapper){
		this.opemapper = opemapper;
	}
	
	public Opettaja haetunnuksella(String tunnus){
		
		String sql = "SELECT * FROM opettaja WHERE username = ?;";
		Object[] parametrit = new Object[] { tunnus };
		RowMapper<Opettaja> mapper = new OpettajaRowMapper();
		
	    Opettaja opettaja;
	    try{
	    opettaja = jdbcTemplate.queryForObject(sql , parametrit, mapper);
	    }catch(Exception e){
	    	return null;
	    }
	    
	    return opettaja;
	                                 
		
		
	}

public Opettaja haeNimella(String nimi){
		
		String sql = "SELECT * FROM opettaja WHERE etunimi = ? AND sukunimi = ?;";
		String etunimi = nimi.substring(0, nimi.indexOf(" ")).trim();
		String sukunimi = nimi.substring(nimi.indexOf(" "), nimi.length()).trim();
		Object[] parametrit = new Object[] { etunimi, sukunimi };
		RowMapper<Opettaja> mapper = new OpettajaRowMapper();
		
	    Opettaja opettaja;
	    try{
	    opettaja = jdbcTemplate.queryForObject(sql , parametrit, mapper);
	    }catch(Exception e){
	    	return null;
	    }
	    
	    return opettaja;
	                                 
		
		
	}
	
	
	public boolean rekisteroi(Opettaja opettaja) {
		final String sql = "INSERT INTO opettaja (etunimi, sukunimi, username, password, enabled) VALUES (?, ?, ?, ?, 1);";
		
		final String etunimi = opettaja.getEtunimi();
		final String sukunimi = opettaja.getSukunimi();
		final String tunnus = opettaja.getTunnus();
		final String salasana = opettaja.getSalasana();
		
		KeyHolder idHolder = new GeneratedKeyHolder();
		//Suoritetaan p�ivitys itse m��rittelem�ll� PreparedStatementCreatorilla
		try{
		jdbcTemplate.update(
	    	    new PreparedStatementCreator() {
	    	        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	    	            PreparedStatement ps = connection.prepareStatement(sql);
	    	            ps.setString(1, etunimi);
	    	            ps.setString(2, sukunimi);
	    	            ps.setString(3, tunnus);
	    	            ps.setString(4, salasana);
	    	            return ps;
	    	        }
	    	    }, idHolder);
	    final String authorityInsert = "INSERT INTO opettaja_authority (opettaja_id, authority_id) VALUES (?, ?);";
	    final int opettajaId = idHolder.getKey().intValue();
	    final int authorityId = 2;
	    jdbcTemplate.update(
	    	    new PreparedStatementCreator() {
	    	        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	    	            PreparedStatement ps = connection.prepareStatement(authorityInsert);
	    	            ps.setInt(1, opettajaId);
	    	            ps.setInt(2, authorityId);
	    	            return ps;
	    	        }
	    	    });
	    
		}catch(Exception e){
			System.out.println(e.getMessage());
			return false;
		}
	    
		
		return true;
	}
	
	public Opettaja haeId(int id){
			
			String sql = "SELECT * FROM opettaja WHERE id = ?;";
			Object[] parametrit = new Object[] { id };
			RowMapper<Opettaja> mapper = new OpettajaRowMapper();
			
		    Opettaja opettaja;
		    try{
		    opettaja = jdbcTemplate.queryForObject(sql , parametrit, mapper);
		    }catch(Exception e){
		    	System.out.println("id haku ei palauttanut mit��n: "+ e);
		    	return null;
		    }
		    return opettaja;
	
	}
	
	public List<Opettaja> haeKaikki(){
		// toimiikohan rowmapper wildcardilla
		String sqlQuery = "SELECT * FROM opettaja;";
		List<Opettaja> opettajalista = jdbcTemplate.query(sqlQuery,opemapper);
		
		return opettajalista;
		
	}
}	
