<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<!-- Väliaikainen bootstrap linkki kokeilua varten,
kokeiltu toistaiseksi navbar.inc kanssa -->
<link rel="stylesheet" href="http://s3.amazonaws.com/codecademy-content/courses/ltp/css/bootstrap.css">


<link href="styles/main.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script src="javascript/script.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Vierailijaluennot</title>
</head>
<body>
<div class="wrapper">
<%@ include file="header.inc" %>
<div class="nav">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="navpad"><a href='<c:url value='/'/>'>Etusivu</a></div>
		</div>
	</div>
</div>
</div>

<div class="container" id="kirjautuminen">
	<form name="f" action="j_spring_security_check" method="POST">
	<fieldset>
		<div class="row">
			<div class="col-md-6 col-xs-6" style="text-align:right">
				Käyttäjätunnus:
			</div>
			<div class="col-md-6 col-xs-6"style="text-align:left">
				<input type="text" name="username"><br>
			</div>
			<div class="col-md-6 col-xs-6" style="text-align:right">
				Salasana:
			</div>
			<div class="col-md-6 col-xs-6"style="text-align:left">
				<input type="password" name="password">
			</div>
		</div>
			<input type="submit" value="Kirjaudu sisään">
	</fieldset>
	</form>
</div>
<div class="row">
<c:if test="${not empty kirjautumisvirhe}">
<div class="virhe">
	<p>Kirjautumisvirhe, yritä uudelleen. Jos ongelma jatkuu, ota yhteyttä järjestelmävalvojaan.</p>
</div>
</c:if>
<c:if test="${not empty uloskirjautuminen }">
<div class="virhe">
	<p>Uloskirjautuminen onnistui. Jos käytät julkista päätettä, tyhjennä selaimen välimuisti ja sulje selain.</p>
</div>
</c:if>
</div>
</div>
<%@ include file="footer.inc" %>
</body>
</html>
