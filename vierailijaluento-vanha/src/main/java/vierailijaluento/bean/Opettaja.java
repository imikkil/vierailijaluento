package vierailijaluento.bean;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.inject.Inject;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Opettaja {
	
	

	private String id;
	private String tunnus;
	private String etunimi;
	private String sukunimi;
	private String kokoNimi;
	private String salasana;
	private String suola;
	
	
	public Opettaja(String etunimi, String sukunimi, String salasana1) {
		super();
		
		
		this.etunimi = etunimi.substring(0,1).toUpperCase() + etunimi.substring(1);
		this.sukunimi = sukunimi.substring(0,1).toUpperCase() + sukunimi.substring(1);
		this.kokoNimi = etunimi + " " + sukunimi;
		
		setTunnus(etunimi.substring(0,1).toLowerCase() + sukunimi.substring(0,4).toLowerCase());
		System.out.println("Opettaja.Opettaja()");
		System.out.println(this.toString());
		System.out.println("Annettu salasana: " + salasana1);
		//Hashataan salasana
		BCryptPasswordEncoder kryptaaja = new BCryptPasswordEncoder();
		this.salasana = kryptaaja.encode(salasana1);
		
	}

	public Opettaja(String id, String tunnus, String etunimi, String sukunimi,
			String salasana, String suola) {
		super();
		this.id = id;
		this.tunnus = tunnus;
		this.etunimi = etunimi;
		this.sukunimi = sukunimi;
		this.salasana = salasana;
		this.suola = suola;
		this.kokoNimi = this.etunimi + " " + this.sukunimi;
	}

	public Opettaja() {
		super();
		
	}
	
	

	public String getKokoNimi() {
		return kokoNimi;
	}

	public void setKokoNimi(String kokoNimi) {
		this.kokoNimi = kokoNimi;
	}

	public String getId() {
		return this.id;
	}
	public int getIdAsInt(){
		return Integer.valueOf(this.id);
	}

	public void setId(String id) {
		this.id = id;

	}

	public String getEtunimi() {
		return etunimi;
	}

	public void setEtunimi(String etunimi) {
		this.etunimi = etunimi;

	}

	public String getSukunimi() {
		return sukunimi;
	}

	public void setSukunimi(String sukunimi) {
		this.sukunimi = sukunimi;

	}

	public String getTunnus() {
		return tunnus;
	}

	public void setTunnus(String tunnus) {
		this.tunnus = tunnus;
	}

	public String getSalasana() {
		return salasana;
	}

	public void setSalasana(String salasana) {
		this.salasana = salasana;
	}

	public String getSuola() {
		return suola;
	}

	public void setSuola(String suola) {
		this.suola = suola;
	}

	@Override
	public String toString() {
		return "Opettaja [id=" + id + ", tunnus=" + tunnus + ", etunimi="
				+ etunimi + ", sukunimi=" + sukunimi + ", kokoNimi=" + kokoNimi
				+ ", salasana=" + salasana + ", suola=" + suola + "]";
	}
	
	public boolean vertaaSalasanaa(String password)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		BCryptPasswordEncoder kryptaaja = new BCryptPasswordEncoder();
		String kryptattuSalasana = kryptaaja.encode(password);
		

		return kryptattuSalasana.equals(this.salasana);
	}
	
	

}
