package vierailijaluento.kontrollerit;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vierailijaluento.bean.Opettaja;
import vierailijaluento.dao.OpettajaDAO;
/**
 * 
 * @author Ilari
 * Kirjautumiskontrollerista l�ytyy liikenteenohjaus ja jonkin verran bisneslogiikkaa.
 * 
 * Liikennett� ohjataan urleissa, joiden sis��n- ja uloskirjautumislogiikan spring security hoitaa.
 * 
 * Ainoa logiikka liittyy rekister�itymiseen.
 * 
 * 
 */
@Controller
public class KirjautumisKontrolleri {
	
	@Inject
	private OpettajaDAO opedao;
	
	public OpettajaDAO getOpettajaDAO(){
		return opedao;
	}
	
	public void setOpettajaDAO(OpettajaDAO opedao){
		this.opedao = opedao;
	}
	 
	@RequestMapping(value="/kirjaudu", method = RequestMethod.GET)
	public String login(Model model) {
 
		return "kirjaudu";
 
	}
	/**
	 * Jos kirjautuminen ei onnistu, n�ytet��n kirjautumisvirhe-sivu.
	 * Spring securityss� on varattu url /kirjautumisvirhe ep�onnistuneelle kirjautumiselle.
	 * Kontrolleri ottaa ko. urlista kiinni, lis�� jsp:lle muuttujan kirjautumisvirhe, ja palauttaa kirjautumissivun.
	 * Kirjautumissivulla tarkistetaan onko ko. muuttuja tyhj� tai null, jos ei ole, n�ytet��n virheilmoitus. Jos on, ilmoitusta ei n�ytet�.
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/kirjautumisvirhe", method = RequestMethod.GET)
	public String loginerror(Model model) {
		
		model.addAttribute("kirjautumisvirhe", "true");
		return "kirjaudu";
 
	}
	/**
	 * Uloskirjautumisessa l�ytyy hyvin v�h�n logiikkaa.
	 * Katsotaan securitycontextista l�ytyyk� autentikointia. Jos autentikointia ei ole (=k�ytt�j� ei ole kirjautunut)
	 * ei kirjata k�ytt�j�� my�sk��n ulos. 
	 * Jos autentikointi l�ytyy (=k�ytt�j� on jossain vaiheessa kirjautunut j�rjestelm��n), kirjataan k�ytt�j� ulos. Spring securityn alta l�ytyy tarvittava metodi t�h�n, joka tuhoaa session ja antaa uuden id:n sessiolle.
	 * 
	 * Tietoturva-aukko muodostuu selainten back-nappulan k�yt�st�, jota k�ytt�m�ll� selain kutsuu omasta v�limuistista aikaisempaa sivua, eik� serverilt�. Eli tietoja voi katsella. Jos jotain yritt�� muuttaa, sovellus vaatii kirjautumisen.
	 * T�m�n eliminoimiseen sovelluksesta l�ytyy filtteri, joka lis�� jokaiseen httpresponseen headerin, joka kertoo selaimelle ett� se ei saa tallentaa v�limuistiin tietoja.
	 * 
	 * @see HeaderFilter 
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/uloskirjautuminen", method = RequestMethod.GET)
	public String logout(Model model, HttpServletRequest request, HttpServletResponse response) {
		    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
		model.addAttribute("uloskirjautuminen", "true");
		return "kirjaudu";
 
	}
	/**
	 * Rekister�itymisen get-kutsussa annetaan jsp-sivulle attribuuttina instanssi opettaja-luokasta. T�m� tiedon k�sittelyn helppouden vuoksi.
	 * Jsp-sivulla oleva jstl-tagi "form" k�ytt�� opettaja-luokkaa tietojen sy�tt�miseen, jotta se voi palauttaa suoraan instanssin opettaja-luokasta,
	 * jossa sy�tetyt tiedot olevat attribuuteissa valmiina. 
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/rekisteroidy", method = RequestMethod.GET)
	public String rekisteroidy(Model model){
		model.addAttribute("opettaja", new Opettaja());
		return "rekisteroi";
	}
	
	/**
	 * Rekister�itymisen POST-kutsun alla on opettajan rekister�itymiseen liittyv� logiikka. Tarkistetaan tietokannasta
	 * onko k�ytt�j� jo rekister�itynyt. Jos on, palautetaan virheilmoituksen kanssa takaisin rekister�itymis-sivulle.
	 * Jos ei, lis�t��n k�ytt�j� tietokantaan. Tunnus tulostetaan k�ytt�j�lle.
	 * 
	 * @param opettaja
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/rekisteroidy", method = RequestMethod.POST)
	public String rekisteroi(@ModelAttribute Opettaja opettaja, Model model){
	
		Opettaja tietokantaOpettaja = new Opettaja(opettaja.getEtunimi(), opettaja.getSukunimi(), opettaja.getSalasana());
		Opettaja testiOpettaja = opedao.haetunnuksella(tietokantaOpettaja.getTunnus());
		if(testiOpettaja == null){
			opedao.rekisteroi(tietokantaOpettaja);
			model.addAttribute("tunnus", tietokantaOpettaja.getTunnus());
			return "rekisterointi-onnistui";
		}else{
			model.addAttribute("rekisteroitymisvirhe", "true");
			return "rekisteroi";
		}
		
	}
	
}
