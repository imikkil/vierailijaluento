package vierailijaluento.dao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class OpettajaRekisteroityminenEiOnnistuPoikkeus extends RuntimeException {
	
	public OpettajaRekisteroityminenEiOnnistuPoikkeus(Exception cause) {
		super(cause);
	}

}
