package vierailijaluento.util;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * HeaderFilter lis�� jokaiseen serverilt� l�htev��n vastaukseen headerit, jotka kertovat selaimelle,
 * ett� mit��n ei tallenneta v�limuistiin, ja kaikki pit�� hakea uudestaan serverilt�. Eli k�yt�nn�ss�
 * eliminoi back-nappulan v��rink�ytt�mahdollisuudet.
 * 
 * @author Ilari
 *
 */

@Component
public class HeaderFilter extends OncePerRequestFilter {
	
	@Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
           	response.addHeader("Cache-control", "no-cache, max-age=0, must-revalidate, no-store");
            
            filterChain.doFilter(request, response);
    }

}


