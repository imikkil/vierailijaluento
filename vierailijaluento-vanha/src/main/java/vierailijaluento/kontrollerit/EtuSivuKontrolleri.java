package vierailijaluento.kontrollerit;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
/**
 * Etusivukontrolleri ohjaa get-kutsut suoraan etusivulle, index.jsp.
 * Post-kutsuissa ohjataan liikenne palautteen n�ytt�mssivulle. index.jsp:ss� on form,
 * johon sy�tetty koodi pit�� k�sitell�, t�m�n takia kutsut erill��n.
 * 
 * @author Ilari
 *
 */
@Controller
public class EtuSivuKontrolleri {

	/**
	 * T�h�n on m�p�tty kaikki mahdolliset urlit, joilta voidaan kutsua etusivua, index.jsp.
	 * @return index
	 */
	@RequestMapping(value = {"","/", "/etusivu", "etusivu"}, method=RequestMethod.GET)
	public String etusivu(){
		return "index";
	}
	
	/**
	 * T�h�n on m�p�tty tarvittavat urlit, joita saatetaan kutsua formin sy�t�n j�lkeen.
	 * Periaatteessa otetaan koodi ja kutsutaan seuraavaa kontrolleria. Tai no, ei "kutsuta"
	 * vaan palautetaan url, jonka spring k�sittelee ja kutsuu url-mappingin mukaisesti seuraavaa kontrolleria.
	 * 
	 * @param model
	 * @param koodi
	 * @return
	 */
	@RequestMapping(value = {"","/", "/etusivu", "etusivu"}, method=RequestMethod.POST)
	public String luentoPalauteOhjaus(Model model, @ModelAttribute String koodi){
		
		return "/palaute?" + koodi;
	}
	
}