package vierailijaluento.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import vierailijaluento.bean.Opettaja;

public class OpettajaRowMapper implements RowMapper<Opettaja> {

	public Opettaja mapRow(ResultSet rs, int rivirno) throws SQLException {
		Opettaja opettaja = new Opettaja();
		opettaja.setId(rs.getString("id"));
		opettaja.setEtunimi(rs.getString("etunimi"));
		opettaja.setSukunimi(rs.getString("sukunimi"));
		opettaja.setTunnus(rs.getString("username"));
		opettaja.setSalasana(rs.getString("password"));
		opettaja.setKokoNimi(rs.getString("etunimi") + " " + rs.getString("sukunimi"));
		return opettaja;
	}

}
