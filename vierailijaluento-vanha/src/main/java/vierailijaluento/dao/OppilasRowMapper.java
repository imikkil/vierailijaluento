package vierailijaluento.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import vierailijaluento.bean.Oppilas;


	public class OppilasRowMapper implements RowMapper<Oppilas> {

		public Oppilas mapRow(ResultSet rs, int rivirno) throws SQLException {
			Oppilas oppilas = new Oppilas();
			oppilas.setEtunimi(rs.getString("etunimi"));
			oppilas.setSukunimi(rs.getString("sukunimi"));
			oppilas.setId(rs.getInt("id"));
			return oppilas;
		}

}

	
