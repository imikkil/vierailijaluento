package vierailijaluento.bean;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Random;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Luento implements Comparator<Luento>, Comparable<Luento> {

  /**
   * Luento-olio pyörittää samaa dataa kuin tietokannasta löytyvä vastaava relaatio.
   * @param id
   * @param luennonaihe
   * @param ajankohta
   * 
   *
   */

  private int id;
  
  @Size(min = 5, max = 255, message="Luennonaiheen t�ytyy olla v�hint��n 5 merkki� pitk�.")
  private String luennonaihe;
  
  
  @Pattern(regexp="(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[012])-(20[1-9][0-9])", message="Anna p�iv�m��r� muodossa pp-kk-vvvv, vuosien 2010-2099 v�lilt�")
  private String pvm;
  
  @Pattern(regexp="([01][0-9]|2[0-3]):[0-5][0-9]", message="Anna aika muodossa hh:mm, 00:00-23:59 v�lilt�")
  private String aika;
  
  private Timestamp ajankohta;
  
  @Size(min = 5, max = 5, message="Luentokoodin t�ytyy olla 5 merkki� pitk�.")
  private String koodi;
  
  @Size(min = 4, max = 255, message="Paikan t�ytyy olla v�hint��n 4 merkki� pitk�.")
  private String paikka;
  
  @Size(min = 5, max = 255, message="Luennoitsijan t�ytyy olla v�hint��n 5 merkki� pitk�.")
  private String luennoitsijastr;
  
  private Luennoitsija luennoitsija;
  
  @Size(min = 1, max = 255, message="Vastuuopettajan t�ytyy l�yty� jo tietokannasta.")
  private String opettajastr;
  
  private Opettaja opettaja;



	/**
	* Default empty Luento constructor
	*/
	public Luento() {
		super();
	}

	public Luento(int id, String luennonaihe, Timestamp ajankohta,
			String koodi, String paikka, String luennoitsijastr,
			Luennoitsija luennoitsija, String opettajastr, Opettaja opettaja) {
		super();
		this.id = id;
		this.luennonaihe = luennonaihe;
		this.ajankohta = ajankohta;
		this.koodi = koodi;
		this.paikka = paikka;
		this.luennoitsijastr = luennoitsijastr;
		this.luennoitsija = luennoitsija;
		this.opettajastr = opettajastr;
		this.opettaja = opettaja;
	}

	/**
	* Default Luento constructor
	*/

	public int getId() {
		return id;
	}

	public Luento(int id, String luennonaihe, Timestamp ajankohta,
			String paikka, Luennoitsija luennoitsija,
			Opettaja opettaja) {
		super();
		this.id = id;
		this.luennonaihe = luennonaihe;
		this.ajankohta = ajankohta;
		this.paikka = paikka;
		this.luennoitsija = luennoitsija;
	}


	public Luento(int id, String luennonaihe, Timestamp ajankohta,
			String koodi, String paikka, Luennoitsija luennoitsija,
			Opettaja opettaja) {
		super();
		this.id = id;
		this.luennonaihe = luennonaihe;
		this.ajankohta = ajankohta;
		this.koodi = koodi;
		this.paikka = paikka;
		this.luennoitsija = luennoitsija;
		this.opettaja = opettaja;
	}

	public String getPaikka() {
		return paikka;
	}

	public void setPaikka(String paikka) {
		this.paikka = paikka;
	}

	public Luennoitsija getLuennoitsija() {
		return luennoitsija;
	}

	public void setLuennoitsija(Luennoitsija luennoitsija) {
		this.luennoitsija = luennoitsija;
	}

	public Opettaja getOpettaja() {
		return opettaja;
	}

	public void setOpettaja(Opettaja opettaja) {
		this.opettaja = opettaja;
	}
	
	public Luento(int id, String luennonaihe, Timestamp ajankohta){
		this.id = id;
		this.luennonaihe = luennonaihe;
		this.ajankohta = ajankohta;
	}

	/**
	* Sets new value of id
	* @param
	*/
	public void setId(int id) {
		this.id = id;
	}

	/**
	* Returns value of luennonaihe
	* @return
	*/
	public String getLuennonaihe() {
		return luennonaihe;
	}

	/**
	* Sets new value of luennonaihe
	* @param
	*/
	public void setLuennonaihe(String luennonaihe) {
		this.luennonaihe = luennonaihe;
	}

	/**
	* Returns value of ajankohta
	* @return
	*/
	public Timestamp getAjankohta() {
		return ajankohta;
	}

	/**
	* Sets new value of Ajankohta
	* @param
	*/
	public void setAjankohta(Timestamp ajankohta) {
		this.ajankohta = ajankohta;
	}

	public String getKoodi() {
		return koodi;
	}

	public void setKoodi(String koodi) {
		this.koodi = koodi;
	}

	@Override
	public String toString() {
		return "Luento [id=" + id + ", luennonaihe=" + luennonaihe
				+ ", ajankohta=" + ajankohta + ", koodi=" + koodi + ", paikka="
				+ paikka + ", luennoitsijastr=" + luennoitsijastr
				+ ", luennoitsija=" + luennoitsija + ", opettajastr="
				+ opettajastr + ", opettaja=" + opettaja + "]";
	}

	public String getLuennoitsijastr() {
		return luennoitsijastr;
	}

	public void setLuennoitsijastr(String luennoitsijastr) {
		this.luennoitsijastr = luennoitsijastr;
	}

	public String getOpettajastr() {
		return opettajastr;
	}

	public void setOpettajastr(String opettajastr) {
		this.opettajastr = opettajastr;
	}

	public String getPvm() {
		return pvm;
	}

	public void setPvm(String pvm) {
		this.pvm = pvm;
	}

	public String getAika() {
		return aika;
	}

	public void setAika(String aika) {
		this.aika = aika;
	}

	public int compare(Luento luento1, Luento luento2) {
		long t1 = luento1.getAjankohta().getTime();
		long t2 = luento2.getAjankohta().getTime();
		
		if(t2 > t1){
			return 1;
		}else if(t1 > t2){
			return -1;
		}else{
			return 0;
		}
	}

	public int compareTo(Luento luento) {
		
		return(this.ajankohta).compareTo(luento.ajankohta);
	}
	
	
	/*
	 * J�rjestelm� vaatii uniikin satunnaiskoodin jokaista luentoa varten.
	 * Alla oleva metodi generoi 5-merkkisen koodin, jossa on isoja ja pieni� kirjaimia sek� numeroita
	 */
	public String generoiKoodi()
	{
		final Random rng = new Random();
		final String characters = "ABCDEFGHIJKLMNOPQRSTUVWXZYabcdefghijlkmnopqrstuvwxzy1234567890";
		final int length = 5;
		
	    char[] text = new char[length];
	    for (int i = 0; i < length; i++)
	    {
	        text[i] = characters.charAt(rng.nextInt(characters.length()));
	    }
	    return new String(text);
	}
	
	
}
