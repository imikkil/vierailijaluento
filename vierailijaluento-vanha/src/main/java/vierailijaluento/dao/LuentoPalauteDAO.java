package vierailijaluento.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.inject.Inject;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;

import vierailijaluento.bean.LuentoPalaute;

@Repository
public class LuentoPalauteDAO {
	
	@Inject
	private JdbcTemplate jdbcTemplate;
	@Inject
	private LuentoPalauteRowMapper luentopalauterowmapper;
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public LuentoPalauteRowMapper getLuentoPalauteRowMapper(){
		return luentopalauterowmapper;
	}
	
	public void setLuentoPalauteRowMapper(LuentoPalauteRowMapper luentopalauterowmapper){
		this.luentopalauterowmapper = luentopalauterowmapper;
	}
	
	public ArrayList<LuentoPalaute> haeLuennonIdlla(int id){
		String sqlQuery= "SELECT * FROM luentopalaute WHERE luentoid = ?;";
		Object[] parametri = new Object[] { id };
		ArrayList<LuentoPalaute> luentopalautelista = (ArrayList<LuentoPalaute>) jdbcTemplate.query(sqlQuery, parametri, luentopalauterowmapper);
		
		return luentopalautelista;
	}
	
	public void lisaa(LuentoPalaute luentopalaute){
		final String sqlInsert = "INSERT INTO luentopalaute (luentoid, oppilasid, kommentti, peukku) VALUES (?, ?, ?, ?);";
		final int luentoId = luentopalaute.getLuentoid();
		final int oppilasId = luentopalaute.getOppilasid();
		final String kommentti = luentopalaute.getKommentti();
		final boolean peukku = luentopalaute.isPeukku();
		
		jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException{
						PreparedStatement ps = connection.prepareStatement(sqlInsert);
						ps.setInt(1, luentoId);
						ps.setInt(2, oppilasId);
						ps.setString(3, kommentti);
						ps.setBoolean(4, peukku);
						return ps;
					}
				});
	}
	
	public void poista(final int id){
		final String sqlDelete = "DELETE FROM luentopalaute WHERE id = ?";
		
		jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException{
						PreparedStatement ps = connection.prepareStatement(sqlDelete);
						ps.setInt(1, id);
						return ps;
					}
				});
	}
}
