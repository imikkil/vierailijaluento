
CREATE TABLE oppilas(
	id CHAR(7) NOT NULL,
	etunimi VARCHAR(40) NOT NULL,
	sukunimi VARCHAR(40) NOT NULL,
	PRIMARY KEY(id)
)engine=InnoDB;

CREATE TABLE opettaja(
	id INT NOT NULL AUTO_INCREMENT,
	etunimi VARCHAR(40) NOT NULL,
	sukunimi VARCHAR(40) NOT NULL,
	username VARCHAR (40) NOT NULL,
	password VARCHAR(255) NOT NULL,
	enabled TINYINT NOT NULL, 
	PRIMARY KEY(id)
)engine=InnoDB;

CREATE TABLE opettaja_authority(
  id integer NOT NULL auto_increment PRIMARY KEY,
  opettaja_id integer NOT NULL,
  authority_id integer NOT NULL,
  FOREIGN KEY (opettaja_id) REFERENCES opettaja(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  FOREIGN KEY (authority_id) REFERENCES authority(id) ON DELETE NO ACTION ON UPDATE NO ACTION	
)engine=InnoDB;

CREATE TABLE authority (
  id integer NOT NULL auto_increment PRIMARY KEY,
  role varchar(255) NOT NULL UNIQUE
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE luennoitsija(
	id BIGINT NOT NULL AUTO_INCREMENT,
	etunimi VARCHAR(40) NOT NULL,
	sukunimi VARCHAR(40) NOT NULL,
	PRIMARY KEY(id)
)engine=InnoDB;

CREATE TABLE luento (
  id INT NOT NULL AUTO_INCREMENT,
  luennonaihe VARCHAR(255) NOT NULL,
  pvm DATETIME NOT NULL,
  vastuuopettajaid INT NOT NULL,
  luennoitsijaid BIGINT NOT NULL,
  paikka VARCHAR(20),
  koodi CHAR(5) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (vastuuopettajaid)
  	REFERENCES opettaja(id),
  FOREIGN KEY (luennoitsijaid)
  	REFERENCES luennoitsija(id)
)engine=InnoDB;

CREATE TABLE luentopalaute(
	id INT NOT NULL AUTO_INCREMENT,
	luentoid INT NOT NULL,
	oppilasid CHAR(7) NOT NULL,
	kommentti VARCHAR(255),
	peukku boolean not null,
	PRIMARY KEY(id),
	FOREIGN KEY(luentoid)
	REFERENCES luento(id),
	FOREIGN KEY (oppilasid)
	REFERENCES oppilas(id)
)engine=InnoDB;
