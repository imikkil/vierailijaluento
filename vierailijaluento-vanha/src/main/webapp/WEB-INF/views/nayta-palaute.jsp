<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 <%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>
<link rel="stylesheet" href="http://s3.amazonaws.com/codecademy-content/courses/ltp/css/bootstrap.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link href="<c:url value='/styles/main.css' />" rel="stylesheet">

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Nayta palaute &middot; ${luento.luennonaihe }</title>

</head>
<body>
<div class="wrapper">
<div class="header">
<h1>Palaute: ${luento.luennonaihe }</h1>
</div>

<div class="nav">
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value='/luento/lista/vanhat' />">Vanhat luennot</a></div>
		</div>
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value='/luento/lista' />">Tulevat luennot</a></div>
		</div>
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value='/luento/lisaaluento' /> ">Lisää luento</a></div>
		</div>
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value="/uloskirjautuminen" /> ">Kirjaudu ulos</a>
		</div>
	</div>
</div>
</div>
</div>
<div class="container">
<div class="row">
	<div class="col-md-12">
		<p>Klikkaa palautteen riviä, jonka kommentin haluat nähdä. Huomioi, että kaikissa palautteissa vapaamuotoista kommenttia ei välttämättä ole.</p>
		<br><br>
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->
<div class="row">
<div class="col-md-6 col-xs-12 col-centered">
	<div class="row" id="luentotiedot">
	<div class="col-md-6 col-xs-6 col-centered">
	<p>Luennon koodi:<br></p>
	<p style="border-bottom: 1px solid;"><c:out value="${luento.koodi }"/></p>
	<p>Luennon aihe:<br></p>
	<p style="border-bottom: 1px solid;"><c:out value="${luento.luennonaihe }"/></p>
	<p>Luennon ajankohta:<br></p>
	<p><fmt:formatDate pattern="dd.MM.yyyy HH:mm" value='${luento.ajankohta}'/><br></p>
	</div> <!-- /col-md-6 -->
	<div class="col-md-6 col-xs-6 col-centered" style="border-left: 1px solid;">
	<p>Luennon paikka:</p>
	<p style="border-bottom: 1px solid;"><c:out value="${luento.paikka }"/><br></p>
	<p>Luennoitsijan nimi:</p>
	<p style="border-bottom: 1px solid;"><c:out value="${luento.luennoitsija.kokoNimi }"/><br></p>
	<p>Vastuuopettajan nimi:</p>
	<p><c:out value="${luento.opettaja.kokoNimi }"/></p>
	</div> <!-- /col-md-6 -->
	</div> <!-- /row -->
</div> <!-- /col-md-6 -->
	<table id="luennot" class="col-md-6 col-xs-12">
	<thead>
		<tr id="tauluhead">
			<th class="taulu">Opiskelijanumero</th>
			<th class="taulu">Tykkäsikö?</th>
			<th class="taulu">Vapaamuotoinen kommentti</th>
			<th class="taulu">Poista palaute</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${luentopalautelista}" var="luentopalaute">
		<form:form method="post" modelAttribute="luentopalaute">
		<tr data-toggle="collapse" data-target="#${luentopalaute.id }" class="accordion-toggle">
			<td class="taulu taulureuna"><c:out value="${luentopalaute.oppilasid}"/></td>
			<td class="taulu"><c:if test="${luentopalaute.peukku }">Kyllä</c:if><c:if test="${!luentopalaute.peukku }">Ei</c:if></td>
			<td class="taulu"><c:if test="${luentopalaute.kommentti != null && !luentopalaute.kommentti.isEmpty() }">Kommentti on</c:if></td>
			<input type="hidden" name="id" value="${luentopalaute.id }">
			<td class="taulu"><input type="submit" value="Poista"></td>
		</tr>
		</form:form>
		<tr>
			<td colspan="6" class="hiddenRow">
				<div class="accordion-body collapse" id="${luentopalaute.id }"><c:if test="${luentopalaute.kommentti != null && !luentopalaute.kommentti.isEmpty() }">${luentopalaute.kommentti }</c:if></div>
			</td> <!-- /hiddenRow -->	
		</c:forEach>
	</tbody>
	</table>
</div> <!-- /row -->
</div> <!-- /container -->
<c:if test="${virheet != null } && ${!virheet.isEmpty()}">
<div class="virhe">
	<c:forEach items="${virheet}" var="virhe">
	<c:out value="${virhe.value}"/>
	</c:forEach>
</div>
</c:if>
</div>
<%@ include file="footer.inc" %>
</body>
</html>
