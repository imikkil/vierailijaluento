package vierailijaluento.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import vierailijaluento.bean.Luennoitsija;
import vierailijaluento.bean.Luento;
import vierailijaluento.bean.Opettaja;


@Repository
public class LuentoDAO {
	
	
	//k�ytet��n springframeworkin jdbctemplatea tietokantayhteyden muodostamiseen 
	@Inject
	private JdbcTemplate jdbcTemplate;
	@Inject
	private LuennoitsijaRowMapper luennoitsijamapper;
	@Inject
	private LuentoRowMapper luentomapper;
	@Inject
	private LuennoitsijaDAO luennoitsijadao;
	@Inject
	private OpettajaDAO opedao;
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public LuennoitsijaDAO getLuennoitsijaDao() {
		return luennoitsijadao;
	}
	
	public void setLuennoitsijaDao(LuennoitsijaDAO luennoitsijadao) {
		this.luennoitsijadao = luennoitsijadao;
	}
	public OpettajaDAO getOpettajaDao() {
		return opedao;
	}
	
	public void setOpettajaDao(OpettajaDAO opedao) {
		this.opedao = opedao;
	}
	//napataan luennoitsijan, luennon ja opettajien row mapperit käyttöä varten
		
	public LuennoitsijaRowMapper getLuennoitsijaRowMapper(){
		return luennoitsijamapper;
	}
	
	public void setLuennoitsijaRowMapper(LuennoitsijaRowMapper luennoitsijamapper){
		this.luennoitsijamapper = luennoitsijamapper;
	}
	
	
	public LuentoRowMapper getLuentoRowMapper(){
		return luentomapper;
	}
	
	public void setLuentoRowMapper(LuentoRowMapper luentomapper){
		this.luentomapper = luentomapper;
	}
	
	
	
	public List<Luento> haeTulevat(){
		// toimiikohan rowmapper wildcardilla
		String sqlquery = "SELECT * FROM luento WHERE pvm >= CURDATE() ORDER BY pvm ASC;";
		if(jdbcTemplate == null){
			System.out.println("JDBC ON NULL :(((((");
			return null;
		}
		final List<Luennoitsija> luennoitsijalista = luennoitsijadao.haeKaikki();
		final List<Opettaja> opettajalista = opedao.haeKaikki();
		List<Luento> luentolista = jdbcTemplate.query(sqlquery,
		 new RowMapper<Luento>() {
			 public Luento mapRow(ResultSet rs, int rowNum) throws SQLException {
			Luento luento = new Luento();
			luento.setId(rs.getInt("id"));
			luento.setLuennonaihe(rs.getString("luennonaihe"));
			luento.setAjankohta(rs.getTimestamp("pvm"));
			luento.setPaikka(rs.getString("paikka"));
			//#SORRYNOTSORRY
			//Self-documenting code ja sillee
			luento.setLuennoitsija(haeLuennoitsijaIdllaValmiistaListasta(Integer.valueOf(rs.getString("luennoitsijaid")), luennoitsijalista));
			luento.setOpettaja(haeOpettajaIdllaValmiistaListasta(Integer.valueOf(rs.getInt("vastuuopettajaid")), opettajalista));
			luento.setKoodi(rs.getString("koodi"));
			System.out.println(luento.toString());
			return luento;
			 }
			 });
		
		
		return luentolista;
		
	}
	
	public List<Luento> haeMenneet(){
		// toimiikohan rowmapper wildcardilla
		String sqlquery = "SELECT * FROM luento WHERE pvm < CURDATE() ORDER BY pvm ASC;";
		if(jdbcTemplate == null){
			System.out.println("JDBC ON NULL :(((((");
			return null;
		}
		final List<Luennoitsija> luennoitsijalista = luennoitsijadao.haeKaikki();
		final List<Opettaja> opettajalista = opedao.haeKaikki();
		List<Luento> luentolista = jdbcTemplate.query(sqlquery,
		 new RowMapper<Luento>() {
			 public Luento mapRow(ResultSet rs, int rowNum) throws SQLException {
			Luento luento = new Luento();
			luento.setId(rs.getInt("id"));
			luento.setLuennonaihe(rs.getString("luennonaihe"));
			luento.setAjankohta(rs.getTimestamp("pvm"));
			luento.setPaikka(rs.getString("paikka"));
			//#SORRYNOTSORRY
			//Self-documenting code ja sillee
			//FITE ME IRL
			luento.setLuennoitsija(haeLuennoitsijaIdllaValmiistaListasta(Integer.valueOf(rs.getString("luennoitsijaid")), luennoitsijalista));
			luento.setOpettaja(haeOpettajaIdllaValmiistaListasta(Integer.valueOf(rs.getInt("vastuuopettajaid")), opettajalista));
			luento.setKoodi(rs.getString("koodi"));
			return luento;
			 }
			 });
		
		
		return luentolista;
		
	}

	public void poista(int id) {
	String sqlDelete = "DELETE FROM luentopalaute WHERE luentoid = ?;";
	String sqlDelete2 = "DELETE FROM luento WHERE id = ?;";
	
	Object[] parametri = { id };
	
	jdbcTemplate.update(sqlDelete, parametri);
	
	jdbcTemplate.update(sqlDelete2, parametri);
	
		
	}
	
	public void lisaa(Luento luento){
		final String sqlInsert = "INSERT INTO luento (luennonaihe, pvm, vastuuopettajaid, luennoitsijaid, paikka, koodi) values (?, ?, ?, ?, ?, ?);";
		
		final String luennonaihe = luento.getLuennonaihe();
		final Timestamp pvm = luento.getAjankohta();
		final String vastuuopettajaid = luento.getOpettaja().getId();
		final int luennoitsijaid = luento.getLuennoitsija().getId();
		final String paikka = luento.getPaikka();
		final String koodi = luento.generoiKoodi();
		
		
		jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException{
						PreparedStatement ps = connection.prepareStatement(sqlInsert);
						ps.setString(1, luennonaihe);
						ps.setTimestamp(2, pvm);
						ps.setString(3, vastuuopettajaid);
						ps.setInt(4, luennoitsijaid);
						ps.setString(5, paikka);
						ps.setString(6, koodi);
						return ps;
					}
				});
	}
	
	public Luento haeId(int id){
		String sqlQuery = "SELECT * FROM luento WHERE id =?;";
		Object[] parametrit = new Object[] { id };
		Luento luento = null;
		try {
			luento = jdbcTemplate.queryForObject(sqlQuery, parametrit, luentomapper);
		} catch (IncorrectResultSizeDataAccessException e) {
			System.out.println("luennoitsijan haku ei onnistu: "+ e);
		}
		
		return luento;
	}
	
	public Luento haeKoodi(String koodi){
		String sqlQuery = "SELECT * FROM luento WHERE koodi =?;";
		Object[] parametrit = new Object[] { koodi };
		Luento luento = null;
		try {
			luento = jdbcTemplate.queryForObject(sqlQuery, parametrit, luentomapper);
		} catch (IncorrectResultSizeDataAccessException e) {
			System.out.println("luennon haku ei onnistunut: "+ e);
		}
		
		return luento;
	}
	
	public void muokkaa(Luento luento){
		final String sqlUpdate = "UPDATE luento SET luennonaihe = ?, pvm = ?, vastuuopettajaid = ?, luennoitsijaid = ?, paikka = ? WHERE id = ?;";
		
		final String luennonaihe = luento.getLuennonaihe();
		final Timestamp pvm = luento.getAjankohta();
		final String vastuuopettajaid = luento.getOpettaja().getId();
		final int luennoitsijaid = luento.getLuennoitsija().getId();
		final String paikka = luento.getPaikka();
		final int id = luento.getId();
		
		jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException{
						PreparedStatement ps = connection.prepareStatement(sqlUpdate);
						ps.setString(1, luennonaihe);
						ps.setTimestamp(2, pvm);
						ps.setString(3, vastuuopettajaid);
						ps.setInt(4, luennoitsijaid);
						ps.setString(5, paikka);
						ps.setInt(6, id);
						return ps;
					}
				});
		
	}
	
	public Luennoitsija haeLuennoitsijaIdllaValmiistaListasta(int id, List<Luennoitsija> luennoitsijalista){
		for(int i = 0; i < luennoitsijalista.size(); i++){
			if(id == luennoitsijalista.get(i).getId()){
				return luennoitsijalista.get(i);
			}
		}
		return null;
		
		
	}
	
	public Opettaja haeOpettajaIdllaValmiistaListasta(int id, List<Opettaja> opettajalista){
		for(int i = 0; i < opettajalista.size(); i++){
			if(id == Integer.valueOf(opettajalista.get(i).getId())){
				return opettajalista.get(i);
			}
		}
		return null;
		
		
	}
	

	
}
