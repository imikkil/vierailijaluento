package vierailijaluento.kontrollerit;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;












import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import vierailijaluento.dao.LuennoitsijaDAO;
import vierailijaluento.dao.LuentoDAO;
import vierailijaluento.dao.LuentoPalauteDAO;
import vierailijaluento.dao.OpettajaDAO;
import vierailijaluento.bean.Luento;
import vierailijaluento.bean.LuentoPalaute;
import vierailijaluento.bean.Opettaja;


/**
 * LuentoKontrollerin sis�ll� on kaikki luentoihin liittyv� frontin k�sittelylogiikka.
 * 
 * T�m� kattaa k�yt�nn�ss� luentojen listaamisen, yksitt�isen luennon n�ytt�misen sek� yksitt�isen luennon muokkaamisen ja lis��misen.
 * 
 * @author Ilari
 *
 */
@Controller
@RequestMapping (value="/luento")
public class LuentoKontrolleri {
	
@Inject
private LuentoDAO luentodao;
@Inject LuennoitsijaDAO luennoitsijadao;

public void setLuentoDao(LuentoDAO luentodao){
	this.luentodao = luentodao;
}

public LuentoDAO getLuentoDao(){
	return luentodao;
}

public void setLuennoitsijaDao(LuennoitsijaDAO luennoitsijadao){
	this.luennoitsijadao = luennoitsijadao;
}

public LuennoitsijaDAO getLuennoitsijaDao(){
	return luennoitsijadao;
}

@Inject
private OpettajaDAO opedao;

public OpettajaDAO getOpettajaDAO(){
	return opedao;
}

public void setOpettajaDAO(OpettajaDAO opedao){
	this.opedao = opedao;
}

@Inject
private LuentoPalauteDAO luentopalautedao;

public LuentoPalauteDAO getLuentoPalauteDAO(){
	return luentopalautedao;
}

public void setLuentoPalauteDAO(LuentoPalauteDAO luentopalautedao){
	this.luentopalautedao = luentopalautedao;
}
		/**
		 * haeLuennot on m�p�tty /lista-urliin. Oletuksena k�ytt�liittym�ss� n�ytet��n vain tulevat luennot, menneet l�ytyv�t erilliselt� sivulta.
		 * Metodi hakee DAO:n avulla kaikki tulevat luennot (pvm-rajaus m��ritelty sql-lauseessa) ja antaa ne jsp-sivulle k�sitelt�v�ksi.
		 * 
		 * @param model
		 * @return
		 */
		@RequestMapping(value="/lista", method=RequestMethod.GET)
		public String haeLuennot(Model model) {
			
			List<Luento> luentolista = luentodao.haeTulevat();
			model.addAttribute("otsikko","Tulevat luennot");
			model.addAttribute("luentolista", luentolista);
			
			return "luentolista";
			
		}
		/**
		 * lista-urlin post-kutsu k�sittelee yksitt�isen luennon poistamisen. Jsp-sivu antaa luento-olion kontrollerille, josta pystyt��n hakemaan id.
		 * T�m� id annetaan DAO:lle, joka hoitaa luennon ja luentoon liittyvien tietojen poistamisen tietokannasta. Mahdollinen jatkokehitysajatus:
		 * Mit� jos luentoa ei poisteta kokonaan, vaan se piilotetaan n�kyvist�? N�in v�hennet��n mahdollisten k�ytt�j�virheiden mahdollisuuksia, eik� data
		 * katoa koskaan. J�rjestelm�nvalvoja/admin pystyisi my�hemmin palauttamaan luennon n�kyviin, jos n�in k�ytt�j� haluaisi.
		 * 
		 * @param model
		 * @param luento
		 * @return
		 */
		@RequestMapping(value="/lista", method=RequestMethod.POST)
		public String poistaLuento(Model model, @ModelAttribute Luento luento){
			
			
			luentodao.poista(luento.getId());
			
			List<Luento> luentolista = luentodao.haeTulevat();
			model.addAttribute("luentolista", luentolista);
			model.addAttribute("luentopoisto", "true");
			return "luentolista";
		}
		/**
		 * haeVanhatLuennot hakee menneet luennot. Muuten toiminnallisuus on t�ysin samanlainen kuin yll�.
		 * 
		 * @see haeLuennot
		 * 
		 * @param model
		 * @return
		 */
		@RequestMapping(value="/lista/vanhat", method=RequestMethod.GET)
		public String haeVanhatLuennot(Model model) {
			
			List<Luento> luentolista = luentodao.haeMenneet();
			model.addAttribute("otsikko","Menneet luennot");
			model.addAttribute("luentolista", luentolista);
			
			return "luentolista";
			
		}
		
		/**
		 * Menneiden luentojen post-kutsu on t�ysin samanlainen kuin tulevien luentojen.
		 * 
		 * @see poistaLuento
		 * @param model
		 * @param luento
		 * @return
		 */
		@RequestMapping(value="/lista/vanhat", method=RequestMethod.POST)
		public String poistaVanhaLuento(Model model, @ModelAttribute Luento luento){
			
			
			luentodao.poista(luento.getId());
			
			List<Luento> luentolista = luentodao.haeMenneet();
			model.addAttribute("luentolista", luentolista);
			model.addAttribute("luentopoisto", "true");
			return "luentolista";
		}
		
		/**
		 * Luentojen lis��ine tapahtuu urin /lisaaluento alla. Get-kutsu hakee tietokannasta opettajat ja luennoitsijat autofill-toiminnallisuuta
		 * varten. Lis�t��n my�s luento-olio jsp-sivun k�ytett�v�ksi, jotta tietojen k�sittely post-kutsussa helpottuu.
		 * 
		 * @param model
		 * @return
		 */
		@RequestMapping(value="/lisaaluento", method=RequestMethod.GET)
		public String lisaaLuentoGet(Model model){
			model.addAttribute("luennoitsijalista", luennoitsijadao.haeKaikki());
			model.addAttribute("opettajalista", opedao.haeKaikki());
			model.addAttribute("luento", new Luento());
			return "lisaa-luento";
		}
		
		/**
		 * Luennon lis��misen post-kutsussa on hiukan jatkokehitt�mist�. Mm. p�iv�m��r�n parsettaminen pit�isi tehd� validoinnin yhteydess�, jotta
		 * virheidenhallinta ei j�� serverin puolelle. T�m� my�skin siivoaisi koodia.
		 * 
		 * Metodi saa tiedot jsp-formilta valmiina luento-oliona. Poislukien ajankohta, joka pit�� erikseen parsettaa timestamp-muotoon, jotta se voidaan sy�tt��
		 * suoraan olion avulla DAO-metodiin.
		 * 
		 * @param model
		 * @param luento
		 * @param request
		 * @param pvmStr
		 * @param kellonaika
		 * @return
		 * @throws ParseException
		 */
		@RequestMapping(value="/lisaaluento", method=RequestMethod.POST)
		public String lisaaLuentoPost (Model model, @ModelAttribute @Valid Luento luento, BindingResult result, HttpServletRequest request,
				@RequestParam(value="pvm")String pvm, @RequestParam(value="aika")String aika) throws ParseException{
			

			

			if (result.hasErrors()) {
				
				model.addAttribute("luennoitsijalista", luennoitsijadao.haeKaikki());
				model.addAttribute("opettajalista", opedao.haeKaikki());
				return "lisaa-luento";
			} else {
				List<Opettaja> opettajalista = opedao.haeKaikki();
				String ajankohtaStr = pvm + " " + aika;
				Timestamp ajankohta = null;
				SimpleDateFormat pvmMuoto = new SimpleDateFormat("dd-MM-yyyy hh:mm");
				Date parseAjankohta = pvmMuoto.parse(ajankohtaStr);
				ajankohta = new Timestamp(parseAjankohta.getTime());
				luento.setLuennoitsija(luennoitsijadao.haeNimella(luento.getLuennoitsijastr()));
				
				Opettaja opettaja = opedao.haeNimella(luento.getOpettajastr());
				if(opettaja == null){
					//TODO: joku virheilmoitus
					return "lisaa-luento";
				}
				for(int i = 0; i < opettajalista.size(); i++){
					//Tarkistetaan t�sm��k� nimi PLEASE DONT HATE ME :(
					if(opettaja.getEtunimi().equalsIgnoreCase(opettajalista.get(i).getEtunimi()) && opettaja.getSukunimi().equalsIgnoreCase(opettajalista.get(i).getSukunimi())){
						luento.setOpettaja(opettajalista.get(i));
						break;
					}
				}
				
				luento.setAjankohta(ajankohta);
				luentodao.lisaa(luento);
				List<Luento> luentolista = luentodao.haeTulevat();
				model.addAttribute("luentolista", luentolista);
				model.addAttribute("luentopoisto", "true");

				return "luentolista";
			}
			


		}
		
		@RequestMapping(value={"/lista/muokkaa/{id}","/muokkaa/{id}"}, method=RequestMethod.GET)
		public String muokkaaLuentoGet(Model model, @PathVariable(value="id") String id){
			int idInt = Integer.valueOf(id);
			Luento muokattavaLuento = luentodao.haeId(idInt);
			String pvmStr = new SimpleDateFormat("dd-MM-yyyy").format(muokattavaLuento.getAjankohta().getTime());
			String kelloStr = new SimpleDateFormat("HH:mm").format(muokattavaLuento.getAjankohta().getTime());
			model.addAttribute("kelloStr", kelloStr);
			model.addAttribute("pvmStr", pvmStr);
			model.addAttribute(muokattavaLuento);
			return "lisaa-luento";
		}
		
		@RequestMapping(value={"/lista/muokkaa/{id}","/muokkaa/{id}"}, method=RequestMethod.POST)
		public ModelAndView muokkaaLuentoPost(Model model, @ModelAttribute Luento luento, @RequestParam(value="kellonaika")String kellonaika, 
				@RequestParam(value="pvm")String pvmStr) throws ParseException{
			

			String ajankohtaStr = pvmStr + " " + kellonaika;
			Timestamp ajankohta = null;
			SimpleDateFormat pvmMuoto = new SimpleDateFormat("dd-MM-yyyy hh:mm");
			Date parseAjankohta = pvmMuoto.parse(ajankohtaStr);
			ajankohta = new Timestamp(parseAjankohta.getTime());
			luento.setLuennoitsija(luennoitsijadao.haeNimella(luento.getLuennoitsijastr()));
			luento.setOpettaja(opedao.haeNimella(luento.getOpettajastr()));
			luento.setAjankohta(ajankohta);
			luentodao.muokkaa(luento);
			
			model.addAttribute("muokkausonnistui", "true");
			
			return new ModelAndView("redirect:/luento/lista");
		}
		
		@RequestMapping(value={"/nayta/{id}", "/lista/nayta/{id}"}, method=RequestMethod.GET)
		public String naytaPalauteGet(Model model, @PathVariable(value="id")String idStr){
			int id = Integer.valueOf(idStr);
			
			Luento luento = luentodao.haeId(id);
			ArrayList<LuentoPalaute> luentopalautelista = luentopalautedao.haeLuennonIdlla(luento.getId());
			
			model.addAttribute("luento", luento);
			model.addAttribute("luentopalautelista", luentopalautelista);
			return "nayta-palaute";
		}
		
		@RequestMapping(value={"/nayta/{id}", "/lista/nayta/{id}"}, method=RequestMethod.POST)
		public String poistaPalaute(Model model, @PathVariable(value="id")String idStr, @ModelAttribute LuentoPalaute luentopalaute){
			int id = luentopalaute.getId();
			
			luentopalautedao.poista(id);
			Luento luento = luentodao.haeId(Integer.valueOf(idStr));
			ArrayList<LuentoPalaute> luentopalautelista = luentopalautedao.haeLuennonIdlla(luento.getId());
			model.addAttribute("luento", luento);
			model.addAttribute("luentopalautelista", luentopalautelista);
			return "nayta-palaute";
		}
		
	
}
