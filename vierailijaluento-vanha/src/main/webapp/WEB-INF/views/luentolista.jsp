<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="http://s3.amazonaws.com/codecademy-content/courses/ltp/css/bootstrap.css">
<link href="<c:url value='/styles/main.css' />" rel="stylesheet">


<script type="text/javascript">
    var luentolista = [
    <c:forEach items="${luentolista}" var="luento" varStatus="status">
    {id: '${luento.id}',
    luennonaihe: '${luento.luennonaihe}',
    ajankohta: '${luento.ajankohta}'
    }
    <c:if test="${!status.last}">
    ,
    </c:if>
    </c:forEach>

    
    ];
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
 <script src="<c:url value='/javascript/lisaaluento.js' />"></script>
<script src="<c:url value='/javascript/script.js' />"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="${otsikko}" > </c:out> &middot; Vierailijaluennot</title>
</head>
<body>
<div class="wrapper">
<h1 class="header">${otsikko}</h1>
<div class="nav">
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value='/luento/lista/vanhat' />">Vanhat luennot</a></div>
		</div>
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value='/luento/lista' />">Tulevat luennot</a></div>
		</div>
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value='/luento/lisaaluento' /> ">Lisää luento</a></div>
		</div>
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value="/uloskirjautuminen" /> ">Kirjaudu ulos</a>
		</div>
	</div>
</div>
</div>
</div>

<div class="container">
<c:if test="${not empty muokkausonnistui }" >
<div>
	<p>Muokkaus onnistui.</p>
</div>
</c:if>
	<c:if test="${empty luentolista }">
	<p>Luentoja ei löytynyt. Tämä voi johtua joko epäonnistuneesta tietokantahausta, tai luentoja ei ole.</p>
	</c:if>
	<table class="col-md-12 luennot">
		<thead>
		<tr id="tauluhead">
			<th class="taulu"><b>Luennon aihe</b></th>
			<th class="taulu"><b>Luennon pvm</b></th>
			<th class="taulu" style="text-align: center;"><b>Toiminnot</b></th>
		</tr>
		</thead>
		<tbody>
		<c:forEach items="${luentolista}" var="luento">
		<form method="post" onsubmit="return confirm('Haluatko varmasti poistaa luennon?')" modelAttribute="luento")>
		<tr id="${luento.id}">
			<input type="hidden" name="id" value="${luento.id }" path="id">
			<td class="taulu taulureuna"><c:out value="${luento.luennonaihe}"/></td>
			<td class="taulu"><fmt:formatDate pattern="dd.MM.yyyy HH:mm" value='${luento.ajankohta}'/></td>
			<td class="taulu"><a href="nayta/${luento.id }">Näytä </a><a href="muokkaa/${luento.id}">Muokkaa </a><input type="submit" name="poista" value="Poista"></td>
		</tr>
		</form>
		</c:forEach>
		</tbody>
		<tr class="blank_row">
			<td colspan="3"></td>
		</tr>
	</table>
</div>
<c:if test="${virheet != null } && ${!virheet.isEmpty()}">
<div class="virhe">
	<c:forEach items="${virheet}" var="virhe">
	<c:out value="${virhe.value}"/>
	</c:forEach>
</div>
</c:if>
</div>
<%@ include file="footer.inc" %>
<script>
$(document).ready(function() {
	console.log(location.pathname);
	  $('.nav a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
	});
</script>
</body>
</html>
