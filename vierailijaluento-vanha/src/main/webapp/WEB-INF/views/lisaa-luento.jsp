<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script type="text/javascript">
    var luennoitsijalista = [
    <c:forEach items="${luennoitsijalista}" var="luennoitsija" varStatus="status">
    {id: '${luennoitsija.id}',
    etunimi: '${luennoitsija.etunimi}',
    sukunimi: '${luennoitsija.sukunimi}'
    }
    <c:if test="${!status.last}">
    ,
    </c:if>
    </c:forEach>

    
    ];
    
    var opettajalista = [
    <c:forEach items="${opettajalista}" var="opettaja" varStatus="status">
    {id: '${opettaja.id}',
    etunimi: '${opettaja.etunimi}',
    sukunimi: '${opettaja.sukunimi}'
    }
    <c:if test="${!status.last}">
    ,
    </c:if>
    </c:forEach>

    
    ];
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<!-- Väliaikainen bootstrap linkki kokeilua varten,
kokeiltu toistaiseksi navbar.inc kanssa --
<link rel="stylesheet" href="http://s3.amazonaws.com/codecademy-content/courses/ltp/css/bootstrap.css">

<!-- Kalenterin vaatimat lisätiedostot -->
	<link rel="stylesheet" type="text/css" href="<c:url value='/calendar/tcal.css' />" />
	<script type="text/javascript" src=<c:url value='/calendar/tcal.js' />></script> 

<!-- pieni kokeilu vaan... -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="<c:url value='/javascript/lisaaluento.js' />"></script>

<link href="<c:url value='/styles/main.css' />" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lisää uusi luento &middot; Vierailijaluennot</title>
</head>
<body>
<div class="wrapper">
<h1 class="header">Lisää uusi luento</h1>
<div class="nav">
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value='/luento/lista/vanhat' />">VaNhaT lUeNnOt</a></div>
		</div>
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value='/luento/lista' />">Tulevat luennot</a></div>
		</div>
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value='/luento/lisaaluento' /> ">Lisää luento</a></div>
		</div>
		<div class="col-md-3">
			<div class="navpad"><a href="<c:url value="/uloskirjautuminen" /> ">Kirjaudu ulos</a>
		</div>
	</div>
</div>
</div>
</div>

<div class="container">
	<div class="row">
<form:form modelAttribute="luento" method="post">

	<div class="col-md-6 col-xs-12">
		<p>Syötä luennon nimi:</p>
		<form:label path="luennonaihe"></form:label>
		<form:input type="text" size="40"  maxlength="40" name="luennonaihe" placeholder="" path="luennonaihe"/><br>
		<form:errors path="luennonaihe" cssStyle="color: #ff0000;" />
		
		<p>Syötä luennon päivämäärä:</p>
		<form:input path="pvm" type="text" name="pvm" class="tcal" value="${pvmStr }"/><br>
		<form:errors path="pvm" cssStyle="color: #ff0000;" />
		
		<p>Syötä luennon aika:</p>
		<form:input path="aika" value="${kelloStr }" type="text" size="5" maxlength="5" name="kellonaika" placeholder="hh:mm" onkeypress='return event.charCode >= 48 && event.charCode <= 58 || event.keyCode==8' /><br>
		<form:errors path="aika" cssStyle="color: #ff0000;" />
	</div>
	
		<!--  Syötä luennon kesto:<br>
		<input type="text"  name="kesto" placeholder="" ><br><br>  LISÄTÄÄN NE KOMMENTIT KOMMENTOINTEIHIN ETTÄ EROTTAA KONFLIKTEISTA ~Jugo -->


	<div class="col-md-6 col-xs-12">
	<div class="ui-widget">
		<p>Anna luennoitsija</p>
		<form:label path="luennoitsijastr"></form:label>
		<form:input value="${luento.luennoitsija.kokoNimi }" id="luennoitsijat" name="luennoitsijat" path="luennoitsijastr" />
	</div>
		<form:errors path="luennoitsijastr" cssStyle="color: #ff0000;" />
		
	<div class="ui-widget">		
		<p>Anna vastuuopettaja</p>
		<form:label path="opettajastr" />
		<form:input value="${luento.opettaja.kokoNimi }" id="opettajat" name="opettajat" path="opettajastr" />
	</div>
		<form:errors path="opettajastr" cssStyle="color: #ff0000;" />
	
	
		<p>Anna paikka</p>
		<form:label path="paikka"></form:label>
		<form:input type="text" name="paikka" path="paikka" />
		<br><form:errors path="paikka" cssStyle="color: #ff0000;" />
		
		<br>
	</div>
		<input type="submit" value="Submit">
</form:form>
	</div>
</div>
<c:if test="${virheet != null } && ${!virheet.isEmpty()}">
<div class="virhe">
	<c:forEach items="${virheet}" var="virhe">
	<c:out value="${virhe.value}"/>
	</c:forEach>
</div>
</c:if>
</div>
<%@ include file="footer.inc" %>
</body>
</html>
