<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ page isELIgnored="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Kalenterin vaatimat lisätiedostot -->
	<link rel="stylesheet" type="text/css" href="calendar/tcal.css" />
	<script type="text/javascript" src="calendar/tcal.js"></script> 
<link rel="stylesheet" href="http://s3.amazonaws.com/codecademy-content/courses/ltp/css/bootstrap.css">
<link href="styles/main.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Muokkaa luentoa</title>

</head>
<body>
<div class="wrapper">
<%@ include file="header.inc" %>
<div class="nav">
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="navpad"><a href="listaaluennot">Luennot</a></div>
		</div>
		<div class="col-md-6">
			<div class="navpad" id="current"><a href="lisaaluento">Lisää luento</a></div>
		</div>
	</div>
</div>
</div>
<div class="container">
<p>Muokkaa luentoa</p>
<form method="post">
	Luennon aihe:<br>
	<input type="text" size="40"  maxlength="40"name="luennonaihe" value="${luento.luennonaihe}"><br>
	Luennon päivämäärä:<br>
	<input type="text" name="pvm" class="tcal" value="<fmt:formatDate pattern="dd-MM-yyyy" value='${luento.ajankohta}'/>" ><br>
	Luennon alkamisajankohta:<br>
	<input type="text" size="5" maxlength="5" name="kellonaika" placeholder="hh:mm" onkeypress='return event.charCode >= 48 && event.charCode <= 58 || event.keyCode==8' value="<fmt:formatDate pattern='HH:mm' value='${luento.ajankohta }'/>" > </input><br>
	Luennon paikka:<br>
	<input type="text" size="16" maxlength="16" name="paikka" value="${luento.paikka}" ><br>
	Luennoitsijan nimi:
	<input type="text" size="20"  maxlength="20" name="luennoitsijanimi" value="${luennoitsija.kokoNimi}" ><br>
	Vastuuopettajan nimi (HUOM TÄMÄ EI TOIMI JOSTAIN SYYSTÄ :( )):
	<input type="text" size="20"  maxlength="20" name="openimi" value="${ope.kokoNimi }"  ><br>
	<input type="hidden" name="id" value="${luento.id }" />
	
	<input type="submit" value="Submit">
</form>
</div>
<c:if test="${virheet != null } && ${!virheet.isEmpty()}">
<div class="virhe">
	<c:forEach items="${virheet}" var="virhe">
	<c:out value="${virhe.value}"/>
	</c:forEach>
</div>
</c:if>
</div>
<%@ include file="footer.inc" %>
</body>
</html>
