<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="stylesheet" href="http://s3.amazonaws.com/codecademy-content/courses/ltp/css/bootstrap.css">
<link href="styles/main.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Vierailijaluennot</title>
</head>
<body>
<div class="wrapper">
<%@ include file="header.inc" %>
<div class="container">
<c:if test="${not empty luentovirhe }">
<div class="error">
	<p>Luentoa ei löytynyt, yritä uudelleen!</p>
</div>
</c:if>
<c:if test="${not empty luentopalaute }">
<div>
	<p>Luentopalautteesi on otettu vastaan, kiitos</p>
</div>
</c:if>
<div class="esittely">
	<p>Sovellus opiskelijoille ja opettajille</p> <br>
		<div class="opiskelijat col-md-6">
			<p><strong>Opiskelijat</strong> saavat antaa palautetta ja ilmoittaa itsensä läsnäolleeksi heille annetun luentokoodin avulla.</p>
			<form:form modelAttribute="koodi" method="get" action="/vierailijaluento/palaute" >
				Hae luento syöttämällä luennon koodi:<br>
				<input type="text" size="5" maxlength="5"name="luentokoodi" placeholder="Koodi" path="koodi"><br>
				<input type="submit" value="Hae">
			</form:form>
		</div>
		<div class="opettajat col-md-6">
			<p><strong>Opettajat</strong> pääsevät hallinnoimaan ja tarkastelemaan vierailijaluentoja. Rekisteröityminen ja sisäänkirjautuminen vaaditaan sovelluksen käyttöön.</p>
			<div class="container login">
				<a href="kirjaudu">Kirjaudu sisään (opettajat)</a><br>
				<a href="rekisteroidy">Rekisteröidy (opettajat)</a>
			</div>
		</div>
</div>
</div>
<br><br><br>


<c:if test="${virheet != null }">
<c:if test="${!virheet.isEmpty()}">
<div class="virhe">
	<c:forEach items="${virheet}" var="virhe">
	<c:out value="${virhe.value}"/>
	</c:forEach>
	
</div>
</c:if>
</c:if>
<div class="push"></div>
</div>
<%@ include file="footer.inc" %>
</body>
</html>
